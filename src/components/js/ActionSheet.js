import * as $ons from 'onsenui';
import firebase from 'firebase/app';
import myProfile from './MyProfile';

let instance;
class ActionSheet {
  /**
     *
     * @param {myProfile} profile
     */
  constructor(profile) {
    this.profile = profile;
    this.db = firebase.firestore();
  }

  static getInstance() {
    if (!instance) instance = new ActionSheet(myProfile.getInstance());
    return instance;
  }

  getActionSheet(item) {
    if (item.user.uid === this.profile.currentUser.uid) {
      $ons.openActionSheet({
        buttons: ['削除', 'キャンセル'],
        title: `${item.user.name} : ${item.content}`,
        destructive: 0,
      }).then((index) => {
        if (index === 0) {
          $ons.notification.confirm({
            message: '削除しますか？',
            callback: ((answer) => {
              if (answer === 1) {
                this.db.collection('timeline').doc(item.id).delete();
                $ons.notification.alert('削除しました');
              }
            }),
          });
        }
      });
    } else {
      $ons.openActionSheet({
        buttons: ['違反を報告', 'キャンセル'],
        title: `${item.user.name} : ${item.content}`,
        destructive: 0,
      }).then((index) => {
        if (index === 0) {
          $ons.notification.confirm({
            message: '報告しますか？',
            callback: ((answer) => {
              if (answer === 1) {
                $ons.notification.alert('報告しました。ご協力ありがとうございます。');
              }
            }),
          });
        }
      });
    }
  }
}

export default ActionSheet;
