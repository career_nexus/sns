import firebase from 'firebase/app';
import uuid4 from 'uuid/v4';
import Jimp from 'jimp';
import router from '../../router';

let instance;

class Profile {
  constructor() {
    this.auth = firebase.auth();
    this.dialogVisible = false;
    this.db = firebase.firestore().collection('users');
    this.storageRef = firebase.storage().ref().child('users');
    this.currentUser = {};
    this.ImageChange = false;
    this.myProfile = {};
  }

  static getInstance() {
    if (!instance) instance = new Profile();
    return instance;
  }

  UpdateCheck() {
    if (this.myProfile.update !== '') {
      router.go(-1);
    } else {
      router.push('/welcome');
    }
  }

  SignOut() {
    this.auth.signOut();
  }

  // カレントユーザを取得して
  async load() {
    await this.auth.onAuthStateChanged((user) => {
      this.PullData(user.uid);
      this.currentUser = user;
    });
  }

  // プロフィール情報を持ってきてキャッシュする
  async PullData(uid) {
    await this.db.doc(uid).get().then((content) => {
      if (this.currentUser.uid === uid) this.myProfile = content.data();
      console.log(this.myProfile);
    });
  }

  // this.Myprofileの上書き
  OverWriting(profile) {
    this.myProfile = profile;
    console.log(this.myProfile);
  }

  // 画像の圧縮
  ImagePress(img) {
    this.dialogVisible = true;
    Jimp.read(img, (error, lenna) => {
      lenna.resize(300, Jimp.AUTO)
        .quality(20).getBase64(Jimp.MIME_JPEG, (err, image) => {
          this.myProfile.img_src = image;
          this.dialogVisible = false;
        });
    });
  }

  // プロフィール画像をstorageへ投げてURLを取得
  async UploadImage() {
    const mountainsRef = this.storageRef.child(this.currentUser.uid);
    const rnd = uuid4();
    const mountainImagesRef = mountainsRef.child(rnd);
    await mountainImagesRef.putString(this.myProfile.img_src, 'data_url');
    this.myProfile.img_src = await mountainImagesRef.getDownloadURL();
    await console.log(this.myProfile);
    this.ImageChange = false;
  }

  // キャッシュにあるデータをDBへ投げる
  async UploadProfile() {
    this.dialogVisible = true;
    const profiles = this.myProfile;
    // 画像の変更があれば画像を保存
    if (this.ImageChange === true) await this.UploadImage();
    profiles.update = firebase.firestore.FieldValue.serverTimestamp();
    this.db.doc(this.currentUser.uid).update(profiles);
    this.dialogVisible = false;
  }
}

export default Profile;
