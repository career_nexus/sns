import * as $ons from 'onsenui';
import firebase from 'firebase/app';
import DisplayProfile from './DisplayProfile';
import router from '../../router';

let instance;
class GetProfile {
  constructor() {
    this.auth = firebase.auth();
    this.db = firebase.firestore()
      .collection('users');
    this.profile = {};
    this.TimelineUser = {};
  }

  static getInstance() {
    if (!instance) instance = new GetProfile();
    return instance;
  }

  get(uid) {
    this.db.doc(uid).get().then((content) => {
      this.profile = content.data();
      if (this.profile === undefined) {
        $ons.notification.alert('情報の取得に失敗しました');
        router.go(-1);
      }
    });
  }

  // UID・名前・画像
  DisplayUserGet(uid) {
    let profile = this.TimelineUser[uid];
    if (!profile) {
      profile = DisplayProfile.from(uid, this.db.doc(uid));
      this.TimelineUser[uid] = profile;
    }
    return profile;
  }
}

export default GetProfile;
