const GenderList = [
  {
    name: '男',
    index: 'man',
  },
  {
    name: '女',
    index: 'woman',
  },
  {
    name: '無回答',
    index: 'none',
  },
];

export default GenderList;
