import firebase from 'firebase/app';
import algoliasearch from 'algoliasearch';
import {
  ALGOLIA_APP_ID, ALGOLIA_USERS_INFO,
  ALGOLIA_MUTTER_INFO, ALGOLIA_ADMIN_KEY,
} from '../../algoliaInit';
import GetProfile from './GetProfile';

const client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_ADMIN_KEY);
const userIndex = client.initIndex(ALGOLIA_USERS_INFO);
const mutterIndex = client.initIndex(ALGOLIA_MUTTER_INFO);

let instance;
class Search {
  /**
     * @param {GetProfile} profile
     */
  constructor(profile) {
    this.db = firebase.firestore().collection('users');
    this.profile = profile;
    this.userList = [];
    this.mutterList = [];
  }

  static getInstance() {
    if (!instance) instance = new Search(GetProfile.getInstance());
    return instance;
  }

  ListCrear() {
    this.userList = [];
    this.mutterList = [];
  }

  // eslint-disable-next-line
  SearchKeyword(keyword) {
    this.ListCrear();
    userIndex.search(keyword, (error, content) => {
      const array = content.hits;
      array.forEach((doc) => {
        this.userList.push(this.UserFormat(doc));
      });
    });
    mutterIndex.search(keyword, (error, content) => {
      const array = content.hits;
      array.forEach((doc) => {
        this.mutterList.push(this.MutterFormat(doc));
      });
    });
  }

  // algoliaから持ってきたデータを他と同じような形式へ変換
  // eslint-disable-next-line
  UserFormat(doc) {
    return {
      uid: doc.objectID,
      name: doc.profile.name,
      img_src: doc.profile.img_src,
    };
  }

  MutterFormat(doc) {
    return {
      id: doc.objectID,
      content: doc.data.content,
      image: doc.data.image,
      date: doc.data.date,
      user: this.profile.DisplayUserGet(doc.data.user),
    };
  }

  // eslint-disable-next-line
    conversion(date) {
    // eslint-disable-next-line
    const d = new Date(date._seconds * 1000);
    // const year = d.getFullYear();
    const month = (`0${d.getMonth() + 1}`).slice(-2);
    const day = (`0${d.getDate()}`).slice(-2);
    const hour = (`0${d.getHours()}`).slice(-2);
    const min = (`0${d.getMinutes()}`).slice(-2);
    // const sec = (`0${d.getSeconds()}`).slice(-2);

    // return `${year}-${month}-${day} T${hour}:${min}:${sec}`;
    return `${month}月${day}日 ${hour}:${min}`;
  }
}

export default Search;
