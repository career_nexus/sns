import Vue from 'vue';
import VueOnsen from 'vue-onsenui';
import VueFire from 'vuefire';
import App from './App.vue';
import router from './router';
import '../node_modules/onsenui/css/onsenui.css';
import '../node_modules/onsenui/css/onsen-css-components.css';

Vue.config.productionTip = false;
Vue.use(VueOnsen);
Vue.use(VueFire);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
