import firebase from 'firebase/app';
import uuid4 from 'uuid/v4';
import Jimp from 'jimp';
import Profile from './MyProfile';

let instance;

class TimelinePost {
  /**
   * @param {Profile} currentUser
   */
  constructor(profile) {
    this.content = '';
    this.dialogVisible = false;
    this.user = profile.currentUser.uid;
    this.img = '';
    this.ref = firebase.storage().ref();
    this.db = firebase.firestore().collection('timeline');
  }

  static getInstance() {
    if (!instance) instance = new TimelinePost(Profile.getInstance());
    return instance;
  }

  async post() {
    this.dialogVisible = true;
    if (this.img !== '') await this.PostImage();
    await this.db.add({
      user: this.user,
      content: this.content,
      image: this.img,
      date: firebase.firestore.FieldValue.serverTimestamp(),
    });
    this.dialogVisible = false;
    await this.clear();
  }

  async clear() {
    this.content = '';
    this.img = '';
  }

  // 画像の圧縮
  ImagePress(img) {
    this.dialogVisible = true;
    Jimp.read(img, (error, lenna) => {
      lenna.resize(600, Jimp.AUTO)
        .quality(70).getBase64(Jimp.MIME_JPEG, (err, image) => {
          this.img = image;
          this.dialogVisible = false;
        });
    });
  }

  async PostImage() {
    const rnd = uuid4();
    // ストレージ場所の参照
    const uploadRef = await this.ref.child('timeline').child(rnd);
    await uploadRef.putString(this.img, 'data_url');
    this.img = await uploadRef.getDownloadURL();
  }
}


export default TimelinePost;
