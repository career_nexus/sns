import firebase from 'firebase/app';
import uuid4 from 'uuid/v4';
import DefaultImage from './DefaultImage';
import router from '../../router';

const actionCodeSettings = {
  url: 'https://careetter.firebaseapp.com/signin',
  handleCodeApp: false,
};

let instance;
class OriginalAuth {
  constructor() {
    this.domain = '@ca.harada-gakuen.ac.jp';
    this.dialogVisible = false;
    this.auth = firebase.auth();
    this.db = firebase.firestore();
    this.storageRef = firebase.storage().ref();
    this.currentUser = {};
    this.input = {
      StudentNumber: '',
      Password: '',
      Message: '',
    };
  }

  static getInstance() {
    if (!instance) instance = new OriginalAuth();
    return instance;
  }

  async GetCurrentUser() {
    // ユーザ情報をcurrentUserに入れる
    this.currentUser = this.auth.currentUser;
  }

  // 入力したデータの初期化
  async Clear() {
    this.input = {
      StudentNumber: '',
      Password: '',
      Message: '',
    };
  }

  async SignIn() {
    try {
      this.dialogVisible = true;
      // サインイン
      await this.auth
        .signInWithEmailAndPassword(this.input.StudentNumber + this.domain, this.input.Password);
      // カレントユーザー取得
      await this.GetCurrentUser();

      if (this.currentUser.emailVerified !== true) {
        this.input.Message = 'Not Verified';
        this.auth.signOut();
        this.dialogVisible = false;
      } else {
        this.input.Message = 'Success!';
        // 初回ログインかチェック
        await this.FirstCheck();
        // 入力データ、エラーメッセージ初期化
        this.dialogVisible = false;
        await this.Clear();
      }
    } catch (error) {
      this.input.Message = error.message;
      this.dialogVisible = false;
    }
  }

  // 初回ログイン時に入力フォームへ飛ばす
  async FirstCheck() {
    const doc = await this.db.collection('users').doc(this.currentUser.uid).get();
    const first = doc.data().update;
    if (!first) {
      router.push('/rules-and-policies');
    } else {
      router.push('/');
    }
  }

  async CreateUser() {
    // ダイアログ開始
    this.dialogVisible = true;
    // 小文字に変換
    this.input.StudentNumber = this.input.StudentNumber.toLowerCase();
    // await console.log(this.input.StudentNumber);

    // 入力した学籍番号の３文字目を取得
    const DepartmentString = this.input.StudentNumber.charAt(2);

    let department;
    // 学科判別
    switch (DepartmentString) {
      case 's':
        department = 'ITスペシャリスト';
        break;

      case 'd':
        department = 'グラフィック';
        break;

      case 'c':
        department = 'TV映像音響';
        break;

      case 'l':
        department = '医療情報管理';
        break;

      case 'm':
        department = 'こども';
        break;

      case 'j':
        department = 'ビューティ';
        break;

      case 'h':
        department = '医療事務';
        break;

      case 'g':
        department = '医療秘書';
        break;

      case 'w':
        department = '日本語';
        break;

      default:
        department = '';
        break;
    }


    if (!department) {
      this.dialogVisible = false;
      this.input.Message = 'Invalid Student Number';
    } else {
      // ユーザの作成
      // eslint-disable-next-line
      await this.auth.createUserWithEmailAndPassword(this.input.StudentNumber + this.domain, this.input.Password).catch((error) => {
        this.dialogVisible = false;
        this.input.Message = error.message;
      });

      // カレントユーザの取得
      this.GetCurrentUser();

      // 文字列をランダム生成
      const rnd = uuid4();
      // storage参照
      const mountainsRef = this.storageRef.child('users').child(this.currentUser.uid);
      const mountainImagesRef = mountainsRef.child(rnd);
      // デフォルトの画像をDBへ投げる
      await mountainImagesRef.putString(DefaultImage.content, 'data_url');
      // ダウンロードURLの取得
      const url = await mountainImagesRef.getDownloadURL();
      await console.log('storage success');

      // DBに投げる
      await this.db.collection('users').doc(this.currentUser.uid).set({
        img_src: url,
        name: '',
        email: this.currentUser.email,
        gender: '',
        circles: [],
        // eslint-disable-next-line
        department: department,
        introduction: '',
        hobby: '',
        free_space: '',
        create: firebase.firestore.FieldValue.serverTimestamp(),
        update: '',
      });
      await console.log('db success');

      // 確認用メール送信
      await this.auth.currentUser.sendEmailVerification(actionCodeSettings);
      await console.log('send Email');

      // ログインした状態になっているのでログアウト
      await this.auth.signOut();
      await console.log('logOut');

      await this.Clear();

      await console.log('go /sent_email');
      // メール送信しました画面へ遷移
      router.push('/sent_email');
      this.dialogVisible = false;
    }
  }
}

export default OriginalAuth;
