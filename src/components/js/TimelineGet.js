import firebase from 'firebase/app';
import GetProfile from './GetProfile';

let instance;
class TimelineGet {
  /**
     * @param {GetProfile} profile
     */
  constructor(profile) {
    this.profile = profile;
    this.db = firebase.firestore().collection('timeline');
    this.entryList = [];
    this.loading = false;
    this.listening = false;
  }

  static getInstance() {
    if (!instance) instance = new TimelineGet(GetProfile.getInstance());
    return instance;
  }

  LoadMore() {
    if (this.listening === true) return;
    this.listening = true;

    const lastEntry = this.entryList.slice(-1)[0];
    this.db
      .where('date', '<', lastEntry.date)
      .orderBy('date', 'desc')
      .limit(100)
      .get()
      .then((querySnapshot) => {
        const entryList = [];
        // ローディングダイアログのための条件分岐です
        if (querySnapshot.docs.length !== 0) {
          this.loading = true;
          setTimeout(() => {
            querySnapshot.forEach((doc) => {
              if (this.entryList.some(entries => entries.id !== doc.id)) {
                entryList.push(this.getDocData(doc));
              }
            });
            this.entryList.splice(this.entryList.length, 0, ...entryList);
            this.loading = false;
            this.listening = false;
          }, 200);
        } else {
          this.listening = false;
        }
      });
  }

  GetTimelineData() {
    this.loading = true;
    this.db
      .orderBy('date', 'desc')
      .limit(100)
      .onSnapshot((snapshot) => {
        const entries = [];
        snapshot.forEach((doc) => {
          entries.push(this.getDocData(doc));
          this.entryList.splice(0, this.entryList.length, ...entries);
        });
        this.loading = false;
      });
  }

  getDocData(doc) {
    const data = doc.data();
    return {
      id: doc.id,
      content: data.content,
      image: data.image,
      date: data.date,
      user: this.profile.DisplayUserGet(data.user),
    };
  }

  // eslint-disable-next-line
  conversion(date) {
    const d = new Date(date.seconds * 1000);
    // const year = d.getFullYear();
    const month = (`0${d.getMonth() + 1}`).slice(-2);
    const day = (`0${d.getDate()}`).slice(-2);
    const hour = (`0${d.getHours()}`).slice(-2);
    const min = (`0${d.getMinutes()}`).slice(-2);
    // const sec = (`0${d.getSeconds()}`).slice(-2);

    // return `${year}-${month}-${day} T${hour}:${min}:${sec}`;
    return `${month}月${day}日 ${hour}:${min}`;
  }
}

export default TimelineGet;
