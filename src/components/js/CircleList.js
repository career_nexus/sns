const CircleList = [
  {
    name: 'バレーボール',
    index: 'volleyball',
  },
  {
    name: 'バスケットボール',
    index: 'basketball',
  },
  {
    name: 'バドミントン',
    index: 'badminton',
  },
  {
    name: 'サッカー・フットサル',
    index: 'soccer',
  },
  {
    name: '軟式野球',
    index: 'baseball',
  },
  {
    name: 'ソフトボール',
    index: 'soft_ball',
  },
  {
    name: 'ソフトテニス',
    index: 'soft_tennis',
  },
  {
    name: 'テニス',
    index: 'tennis',
  },
  {
    name: '卓球',
    index: 'table_tennis',
  },
  {
    name: 'ボウリング',
    index: 'bowling',
  },
  {
    name: 'ビリヤード',
    index: 'billiards',
  },
  {
    name: '軽音楽',
    index: 'music',
  },
  {
    name: 'メディア・アーツ',
    index: 'media_arts',
  },
  {
    name: '吹奏楽部',
    index: 'brass_band_club',
  },
  {
    name: 'ダンス',
    index: 'dance',
  },
  {
    name: '演劇',
    index: 'theater',
  },

  {
    name: '弓道',
    index: 'japanese_archery',
  },
  {
    name: 'ゲーム制作',
    index: 'game',
  },
  {
    name: '囲碁・将棋',
    index: 'igo_shogi',
  },
  {
    name: '幼児活動',
    index: 'child',
  },
  {
    name: '着付け',
    index: 'kimono',
  },
  {
    name: 'ビューティ',
    index: 'beauty',
  },
  {
    name: 'ボランティア',
    index: 'volunteer',
  },
  {
    name: 'コンピュータ',
    index: 'computer',
  },
  {
    name: 'その他',
    index: 'other',
  },
];

export default CircleList;
