class DisplayProfile {
  constructor(uid, name, img) {
    this.uid = uid;
    this.name = name;
    this.img = img;
  }

  static from(uid, db) {
    const profile = new DisplayProfile(uid, uid, uid);

    db.onSnapshot((doc) => {
      if (doc.exists) {
        profile.name = doc.data().name;
        profile.img = doc.data().img_src;
      }
    });
    return profile;
  }
}

export default DisplayProfile;
