import Vue from 'vue';
import firebase from 'firebase/app';
import Router from 'vue-router';
import Home from './views/Home.vue';

import ProfileSetting from './views/ProfileSetting.vue';
import NotFoundComponent from './views/NotFoundComponent.vue';
import Signup from './views/Signup.vue';
import SentEmail from './views/SentEmail.vue';
import Signin from './views/SignIn.vue';
import WorkingSpace from './views/WorkingSpace.vue';
import Profile from './views/Profile.vue';
import TimelinePost from './views/TimelinePost.vue';
import Welcome from './views/WelcomeDialog.vue';
import Timeline from './views/Timeline.vue';
import Search from './views/Search.vue';
import Auth from './views/Auth.vue';
import TimelineEntry from './views/TimelineEntry.vue';
import Help from './views/Help.vue';
import ReSetting from './views/ReSetting.vue';
import Send from './views/Send.vue';
import Rules from './views/Rules.vue';
import Manual from './views/Manual.vue';

Vue.use(Router);

// ログインコンテンツ以外は meta: { NotNeedAuth: true }, をつける
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      component: NotFoundComponent,
      meta: { NotNeedAuth: true },
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        // NotNeedAuth: true,
        title: 'ホーム',
      },
    },
    {
      path: '/timeline',
      name: 'Timeline',
      component: Timeline,
      meta: {
        // NotNeedAuth: true,
        title: 'タイムライン',
      },
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
      meta: { NotNeedAuth: true },
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
      meta: {
        NotNeedAuth: true,
        title: 'サインアップ',
      },
    },
    {
      path: '/sent_email',
      name: 'SentEmail',
      component: SentEmail,
      meta: {
        NotNeedAuth: true,
        title: '',
      },
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin,
      meta: {
        NotNeedAuth: true,
        title: 'サインイン',
      },
    },
    {
      path: '/search',
      name: 'Search',
      component: Search,
      meta: {
        title: '検索',
      },
    },
    {
      path: '/profile_setting',
      name: 'ProfileSetting',
      component: ProfileSetting,
      meta: {
        title: 'プロフィール設定',
      },
    },
    {
      path: '/working_space',
      name: 'WorkingSpace',
      component: WorkingSpace,
      meta: {
        NotNeedAuth: true,
        title: '確認用',
      },
    },
    {
      path: '/profile/:id',
      name: 'Profile',
      component: Profile,
      /*
      meta: {
        title: 'のプロフィール',
      },
      */
    },
    {
      path: '/timeline_post',
      name: 'TimelinePost',
      component: TimelinePost,
      meta: {
        title: 'つぶやき',
      },
    },
    {
      path: '/welcome',
      name: 'Welcome',
      component: Welcome,
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth,
      meta: {
        NotNeedAuth: true,
        title: 'ようこそ',
      },
    },
    {
      path: '/timeline/:id',
      name: 'Entry',
      component: TimelineEntry,
      meta: {
        title: '',
      },
    },
    {
      path: '/help',
      name: 'Help',
      component: Help,
      meta: {
        NotNeedAuth: true,
      },
    },
    {
      path: '/resetting',
      name: 'ReSetting',
      component: ReSetting,
      meta: {
        NotNeedAuth: true,
      },
    },
    {
      path: '/send',
      name: 'Send',
      component: Send,
      meta: {
        NotNeedAuth: true,
      },
    },
    {
      path: '/rules-and-policies',
      name: 'RulesAndPolicies',
      component: Rules,
      meta: {
        NotNeedAuth: true,
      },
    },
    {
      path: '/manual',
      name: 'Manual',
      component: Manual,
      meta: {
        NotNeedAuth: true,
      },
    },
  ],
});

// ログインしていない場合は'/auth'に飛ばす
router.beforeEach((to, from, next) => {
  const NotNeedAuth = to.matched.some(record => record.meta.NotNeedAuth);
  if (!NotNeedAuth) {
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        next({
          path: '/auth',
          query: { redirect: to.fullPath },
        });
      } else {
        next();
      }
    });
  } else {
    next();
  }
});

export default router;
