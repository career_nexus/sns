import firebase from 'firebase/app';
import GetProfile from './GetProfile';

let instance;
class EntryData {
  constructor(profile) {
    this.db = firebase.firestore();
    this.getProfile = profile;
    this.EntryData = {};
    this.loading = false;
  }

  static getInstance() {
    if (!instance) instance = new EntryData(GetProfile.getInstance());
    return instance;
  }

  async GetEntryData(id) {
    this.loading = true;

    await this.db.collection('timeline').doc(id).get().then((doc) => {
      this.EntryData = this.EntryDataFormat(doc);
      this.GetComments(id);
      console.log(this.EntryData);
      this.loading = false;
    });
  }

  EntryDataFormat(doc) {
    const data = doc.data();
    return {
      id: doc.id,
      user: this.getProfile.DisplayUserGet(data.user),
      content: data.content,
      image: data.image,
      date: this.conversion(data.date),
      comments: '',
    };
  }

  GetComments(id) {
    this.db.collection('timeline').doc(id)
      .collection('comments').orderBy('date', 'desc')
      .onSnapshot((snapshot) => {
        const comments = [];
        snapshot.forEach((doc) => {
          comments.push(this.CommentFormat(doc));
        });
        this.EntryData.comments = comments;
      });
  }

  CommentFormat(doc) {
    const data = doc.data();
    return {
      id: doc.id,
      user: this.getProfile.DisplayUserGet(data.user),
      comment: data.comment,
      date: this.conversion(data.date),
    };
  }

  // eslint-disable-next-line
  conversion(date) {
    const d = new Date(date.seconds * 1000);
    // const year = d.getFullYear();
    const month = (`0${d.getMonth() + 1}`).slice(-2);
    const day = (`0${d.getDate()}`).slice(-2);
    const hour = (`0${d.getHours()}`).slice(-2);
    const min = (`0${d.getMinutes()}`).slice(-2);
    // const sec = (`0${d.getSeconds()}`).slice(-2);

    // return `${year}-${month}-${day} T${hour}:${min}:${sec}`;
    return `${month}月${day}日 ${hour}:${min}`;
  }
}

export default EntryData;
