import * as $ons from 'onsenui';
import firebase from 'firebase/app';
import MyProfile from './MyProfile';

let instance;
class Comment {
  /**
     *
     * @param {MyProfile}
     */
  constructor(profile) {
    this.profile = profile;
    this.user = profile.currentUser.uid;
    this.comment = '';
    this.db = firebase.firestore().collection('timeline');
  }

  static getInstance() {
    if (!instance) instance = new Comment(MyProfile.getInstance());
    return instance;
  }

  post(id) {
    this.db.doc(id).collection('comments').add({
      user: this.user,
      comment: this.comment,
      date: firebase.firestore.FieldValue.serverTimestamp(),
    });
    this.clear();
  }

  editComment(id, item) {
    if (item.user.uid === this.user) {
      $ons.openActionSheet({
        buttons: ['削除', 'キャンセル'],
        title: `${item.user.name} : ${item.comment}`,
        destructive: 0,
      }).then((index) => {
        if (index === 0) {
          $ons.notification.confirm({
            message: '削除しますか？',
            callback: ((answer) => {
              if (answer === 1) {
                this.db.doc(id).collection('comments').doc(item.id).delete();
                $ons.notification.alert('削除しました。');
              }
            }),
          });
        }
      });
    } else {
      $ons.openActionSheet({
        buttons: ['違反を報告', 'キャンセル'],
        title: `${item.user.name} : ${item.comment}`,
        destructive: 0,
      }).then((index) => {
        if (index === 0) {
          $ons.notification.confirm({
            message: '報告しますか？',
            callback: ((answer) => {
              if (answer === 1) {
                $ons.notification.alert('報告しました。ご協力ありがとうございます。');
              }
            }),
          });
        }
      });
    }
  }

  clear() {
    this.comment = '';
  }
}

export default Comment;
