import firebase from 'firebase/app';

let instance;
class ReSetting {
  constructor() {
    this.auth = firebase.auth();
    this.studentnumber = '';
    this.domain = '@ca.harada-gakuen.ac.jp';
    this.message = '';
  }

  static GetInstance() {
    if (!instance) instance = new ReSetting();
    return instance;
  }

  async SendEmail() {
    this.auth.sendPasswordResetEmail(this.studentnumber + this.domain)
      .then(() => {
        console.log('send email');
      }).catch((error) => {
        console.log(error.message);
      });
  }

  async Crear() {
    this.studentnumber = '';
  }
}

export default ReSetting;
