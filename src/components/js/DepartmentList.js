const DepartmentList = [
  {
    id: 's',
    name: 'ITスペシャリスト',
  },
  {
    id: 'd',
    name: 'グラフィック',
  },
  {
    id: 'c',
    name: 'TV映像音響',
  },
  {
    id: 'm',
    name: 'こども',
  },
  {
    id: 'j',
    name: 'ビューティ',
  },
  {
    id: 'l',
    name: '医療情報管理',
  },
  {
    id: 'g',
    name: '医療福祉秘書',
  },
  {
    id: 'h',
    name: '医療福祉事務',
  },
  {
    id: 'w',
    name: '日本語',
  },
];

export default DepartmentList;
