const firebase = require('firebase/app');

require('firebase/auth');
require('firebase/storage');
require('firebase/firestore');

const config = {
  apiKey: 'AIzaSyDscZCAKGL8aKAs1sbagIzGd-DxTtY23kI',
  authDomain: 'careetter.firebaseapp.com',
  databaseURL: 'https://careetter.firebaseio.com',
  projectId: 'careetter',
  storageBucket: 'careetter.appspot.com',
  messagingSenderId: '948097028722',
};
firebase.initializeApp(config);

firebase.firestore().settings({
  timestampsInSnapshots: true,
});
